library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
--use UNISIM.VComponents.all;

-- VD: capture signals in 2 consecutive clock windows and keep them up indefinitely

entity Latch2Bits is
 Port ( 
  i_clk  : in  std_logic;
--  i_enb  : in  std_logic;

--input trigger signal 
  i_sigO : in  std_logic;
  
--input vector: they will be copied to the output for 2 clk cycles
  i_sigV : in  std_logic_vector(7 downto 0);
  
--out: veto signal [when aquiring the second window]
  o_outReady: out std_logic;
--out: containes 2 window of the initial signals
  o_out  : out std_logic_vector(23 downto 0) --was 15
 
 );
end Latch2Bits;


architecture Main_2w of Latch2Bits is

 signal m_out, m_out2   : std_logic_vector(23 downto 0) := (others => '0');
 signal m_count         : natural range 0 to 2 := 0;    
 signal m_temp_out      : std_logic_vector(7 downto 0)  := (others => '0');

 signal m_outReady      : std_logic := '0';
 
begin

 process (i_clk) --, i_enb) 
 begin
  
 o_outReady <= m_outReady;
 if rising_edge(i_clk) then
  if i_sigO='1' and m_count=0 then
    m_out(7 downto 0)  <= i_sigV;
    m_out(15 downto 8) <= m_temp_out;
    m_count <= 1;
    m_outReady <= '0';
  elsif m_count=1 then
    m_out(15 downto 8) <= i_sigV;
    m_count <= 2;
    m_outReady <= '1';
  else 
    m_count <= 0;
    m_outReady <= '0';
  end if;
 end if;
end process;

 o_out <= m_out;
 
end Main_2w;

----------------------------------------------------------------------------------------------------
architecture Main_3w of Latch2Bits is

 signal m_out, m_out2   : std_logic_vector(23 downto 0) := (others => '0');
 signal m_count         : natural range 0 to 3 := 0;    
 signal m_temp_out      : std_logic_vector(7 downto 0)  := (others => '0');

 signal m_outReady      : std_logic := '0';
 
begin

 process (i_clk) --, i_enb) 
 begin
  
 o_outReady <= m_outReady;
 if rising_edge(i_clk) then
  if i_sigO='1' and m_count=0 then
    m_out( 7 downto  0) <= i_sigV;
    m_out(15 downto  8) <= m_temp_out;
    m_out(23 downto 16) <= m_temp_out;
    m_count <= 1; 
    m_outReady <= '0';
  elsif m_count=1 then
    m_out(15 downto  8) <= i_sigV;
    m_out(23 downto 16) <= m_temp_out;
    m_count <= 2;
  elsif m_count=2 then
    m_out(23 downto 16) <= i_sigV;
    m_count <= 3;
    m_outReady <= '1';  
  else 
    m_count <= 0;
    m_outReady <= '0';
  end if;
 end if;
end process;


 o_out <= m_out;
 
end Main_3w;