library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package readout_constants is

 constant NSLAVES        : positive := 100;
 
 constant IPBADDR_VERSION  : natural :=  0;
 constant IPBADDR_STATUS   : natural :=  1;
 constant IPBADDR_RESET    : natural :=  2;
 constant IPBADDR_BUSY     : natural :=  3;
 constant IPBADDR_SCFIFO_R : natural :=  4;
 constant IPBADDR_SCFIFO_W : natural :=  5;
 constant IPBADDR_AOFIFO   : natural :=  6;
 constant IPBADDR_AOMONI   : natural :=  7;
 constant IPBADDR_DUMCTR   : natural :=  8;
 constant IPBADDR_DELAY    : natural :=  9;

 constant AO_ADDR_TAPW   : natural :=10;
 constant AO_ADDR_TAPR   : natural :=50;
 
 constant AO_ADDR_WDCTR  : natural := 91;
 constant AO_ADDR_DELAY  : natural := 92;

-------------------------------------------------------------------------------------------------- 
 constant NPINS          : natural := 37;
 constant IPBUS_WIDTH    : natural := 32;
 constant FASTFIFO_WIDTH : natural := 62; -- bojan 44->62, modified also in fifo_Fast IP
 constant DIFF_WIDTH     : natural := 30; -- 12->30
 
 -- bojan
 constant TRIG_ID_SIZE      : natural := 18;    
 constant FINAL_WORD_LENGTH : natural := 62;

 type MatrixW is array (0 to NPINS-1) of std_logic_vector(23 downto 0);
end;
