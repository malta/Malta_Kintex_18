library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.readout_constants.all;

-- conventions: 
---  * fast fifo is called _1
---  * ipbus fifo is called _2
---  * this only assumed reading operatoins
---  * the same clk is used to read to one fifo and writ to the next

entity FifoConnector is
 Port ( 
  i_clk         : in std_logic;
 
  i_fifo1_empty : in std_logic;
  --i_fifo2_empty : in std_logic;
 
  --i_fifo1_full  : in std_logic;
  i_fifo2_almost: in std_logic;
  
  o_fifo1_rdEn  : out std_logic;
  o_fifo2_wrEn  : out std_logic;
  
  i_fifo1_out   : in  std_logic_vector( FASTFIFO_WIDTH-1 downto 0 );
  o_fifo2_in    : out std_logic_vector( IPBUS_WIDTH-1    downto 0 )
 
 );
end FifoConnector;

architecture Main of FifoConnector is
  
 signal m_fifo1_rdEn : std_logic := '0';
 signal m_fifo2_wrEn : std_logic := '0';
 
 signal m_fifo1_out  : std_logic_vector( FASTFIFO_WIDTH-1 downto 0 ) := (others => '0');
-- signal m_fifo2_in_1 : std_logic_vector( IPBUS_WIDTH-1    downto 0 ) := (others => '0');
 signal m_fifo2_in_2 : std_logic_vector( IPBUS_WIDTH-1    downto 0 ) := (others => '0');
 
 signal m_state      : natural range 0 to 4 := 0; 
 
begin
-- atm doing thing very slowly ... probably means that I could be loosing few clk cycle ....
-- also, need to add the first bit protection!!!!!!!
 o_fifo1_rdEn <= m_fifo1_rdEn;
 o_fifo2_wrEn <= m_fifo2_wrEn;
 
 process (i_clk) is 
  begin  
  if rising_edge(i_clk) then  

   if m_state=0 and i_fifo1_empty='0' and i_fifo2_almost='0' then
    -- enable the reading
    m_fifo1_rdEn <= '1';
    m_state <= 1;
    
   elsif m_state=1 then
    m_fifo1_rdEn <= '0';
    m_state <= 2;
   
   elsif m_state=2 then
    --finally I have my word!!
    -- copying 31 (and not 32!!!) bits in the first word to leave room for the debugging bit!!!
--    o_fifo2_in                           <= i_fifo1_out(IPBUS_WIDTH-1 downto 0);
--    m_fifo2_in_2( DIFF_WIDTH-1 downto 0) <= i_fifo1_out(FASTFIFO_WIDTH-1 downto IPBUS_WIDTH);
    o_fifo2_in(IPBUS_WIDTH-2 downto 0) <= i_fifo1_out(IPBUS_WIDTH-2 downto 0);
    m_fifo2_in_2( DIFF_WIDTH downto 0) <= i_fifo1_out(FASTFIFO_WIDTH-1 downto IPBUS_WIDTH-1);
    m_fifo2_wrEn <= '1';
    m_state <= 3;
   
   elsif m_state=3 then
    m_fifo2_wrEn <= '1'; 
    o_fifo2_in   <= m_fifo2_in_2;
    m_state <= 4;
   
   else 
    o_fifo2_in   <= (others => '0');
    m_fifo2_in_2 <= (others => '0');
    m_fifo1_out  <= (others => '0');
    m_fifo2_wrEn <= '0';
    m_state <=0;
    
   end if;
  
  end if;
 end process;

end Main;
