library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.package_constants.all;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

entity serializer is
port(
    i_clock        : in std_logic;
    i_dataParallel : in std_logic_vector(NBITS-1 downto 0);
    o_dataSerial   : out std_logic;    
    i_start        : in std_logic;
    o_busy         : out std_logic
);
end serializer;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

architecture ARCH1_Serializer of serializer is

    --------
    -- wires
    --------
    
    signal m_i_clock        : std_logic := '0';
    signal m_i_dataParallel : std_logic_vector(NBITS-1 downto 0) := NULLWORD;
    signal m_o_dataSerial   : std_logic := '1';
    signal m_i_start        : std_logic := '0';
    signal m_o_busy         : std_logic := '0';
    
    -------------------------
    -- counter: 16+1 bit word
    -------------------------
    
    signal m_counter_bitsInWord : natural range 0 to NBITS+1 := 0; -- cycle 0 -> send 0, cycle NBITS+1 -> reset

begin

    ----------
    -- process
    ----------

    process_serializer : process (m_i_clock) is
    begin
        if rising_edge(m_i_clock) then
                                
            if m_o_busy = '0' then -- if not busy, if receiving start signal, go busy
                if m_i_start = '1' then
                    m_o_busy <= '1';
                end if;
            else -- if busy, transmit bits
                if m_counter_bitsInWord = NBITS+1 then -- last clock cycle used to unset busy, put serial line to '1' and reset bit counter
                    m_o_busy <= '0';
                    m_o_dataSerial <= '1';
                    m_counter_bitsInWord <= 0;                
                else 
                    if m_counter_bitsInWord = 0 then -- if first bit, first clock cycle used to send a '0'
                        m_o_dataSerial <= '0';
                    else
                        m_o_dataSerial <= m_i_dataParallel(m_counter_bitsInWord-1);
                    end if;
                    m_counter_bitsInWord <= m_counter_bitsInWord + 1;
                end if;                 
            end if;
                
        end if;        
            
    end process process_serializer;

    ---------
    -- inputs
    ---------
    
    m_i_clock        <= i_clock;
    m_i_start        <= i_start;
    m_i_dataParallel <= i_dataParallel;

    ----------
    -- outputs
    ----------
    
    o_busy       <= m_o_busy;
    o_dataSerial <= m_o_dataSerial;  

end ARCH1_Serializer;
