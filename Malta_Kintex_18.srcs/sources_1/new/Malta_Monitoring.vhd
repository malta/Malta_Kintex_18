----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2018 10:46:51 AM
-- Design Name: 
-- Module Name: Malta_Monitoring - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;
use work.readout_constants.all;

entity Malta_Monitoring is
  Port ( 
  
    i_ClkToMalta          : in std_logic;
    
    i_toMalta             : in std_logic;
    i_FMC_FROMMALTA_P     : in std_logic;
    i_FMC_FROMMALTA_N     : in std_logic;
    
    i_vpulse_toMalta      : in std_logic;
    
    i_TRIG_TOMALTA        : in std_logic;
    
    i_RST_TOMALTA         : in std_logic;
    
    i_DATA_PIN            : in std_logic_vector(NPINS-1 downto 0) := (others => '0'); 
    
    o_RST_TOMALTA         : out std_logic;
    
    o_CLK_TOMALTA_P       : out std_logic;
    o_CLK_TOMALTA_N       : out std_logic;
                                      
    o_FMC_TOMALTA_P       : out std_logic;
    o_FMC_TOMALTA_N       : out std_logic;
    
    o_FROMMALTA           : out std_logic;
    
    o_VPULSE_TOMALTA_P    : out std_logic;
    o_VPULSE_TOMALTA_N    : out std_logic;
    
    o_TRIG_TOMALTA_P      : out std_logic;
    o_TRIG_TOMALTA_N      : out std_logic
    
--    o_FMC2_SC_FROMMALTA_P : out std_logic;
--    o_FMC2_SC_TOMALTA_P   : out std_logic;
    
--    o_FMC2_PIN_P          : out std_logic_vector(1 downto 0);
--    o_FMC2_PIN_N          : out std_logic_vector(1 downto 0)   
  );
end Malta_Monitoring;

architecture Behavioral of Malta_Monitoring is

 signal m_Pixels_monitor : std_logic_vector(1 downto 0);
 signal m_fromMalta      : std_logic;

begin

  OBUF_reset : OBUF
  generic map (
    DRIVE => 4, --2, --12,
    IOSTANDARD => "DEFAULT",
    SLEW => "SLOW")
  port map (
    O => o_RST_TOMALTA,
    I => i_RST_TOMALTA
  );
 
-------------------------------------------------------------------------------------------------------------------------- 
  OBUFDS_clk : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "FAST")    
  port map (
    O  => o_CLK_TOMALTA_P,
    OB => o_CLK_TOMALTA_N,
    I  => i_ClkToMalta
  );  
     
-------------------------------------------------------------------------------------------------------------------------- 
  OBUFDS_toMALTA : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "SLOW")
  port map (
    O  => o_FMC_TOMALTA_P,
    OB => o_FMC_TOMALTA_N,
    I  => i_toMALTA           
  );
 
-------------------------------------------------------------------------------------------------------------------------- 
  IBUFDS_inst : IBUFDS
  generic map (
    DIFF_TERM    => TRUE, 
    IBUF_LOW_PWR => TRUE,
    IOSTANDARD   => "LVDS")
  port map (
    O  => m_fromMALTA,       
    I  => i_FMC_FROMMALTA_P,
    IB => i_FMC_FROMMALTA_N
  ); 
  o_fromMALTA <= m_fromMALTA;
  
--------------------------------------------------------------------------------------------------------------------------    
  OBUFDS_VPulse : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT",
     SLEW       => "SLOW")
  port map (
    O  => o_VPULSE_TOMALTA_P,
    OB => o_VPULSE_TOMALTA_N,
    I  => i_vpulse_toMalta 
  );
   
-------------------------------------------------------------------------------------------------------------------------- 
  OBUFDS_Trigger : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT",  
    SLEW       => "SLOW")
  port map (
    O  => o_TRIG_TOMALTA_P,
    OB => o_TRIG_TOMALTA_N,
    I  => i_TRIG_TOMALTA
  );
      

-------------------------------------------------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------------------------------------------------- 
-------------------------------------------------------------------------------------------------------------------------- 
-------------------  FMC2 Monitoring  ------------------------------------------------------------------------------------
--   OBUFDS_FMC2FROMMALTA : OBUFDS
--    generic map (
--       IOSTANDARD => "DEFAULT",  -- Specify the output I/O standard
--       SLEW       => "SLOW")     -- Specify the output slew rate
--    port map (
--       O  => o_FMC2_SC_FROMMALTA_P,   -- Diff_p output (connect directly to top-level port)
--       OB => o_FMC2_SC_FROMMALTA_N,   -- Diff_n output (connect directly to top-level port)
--       I  => m_fromMalta      -- Buffer input 
--    );

--  OBUF_FMC2FROMMALTA : OBUF
--   generic map (
--      --DRIVE => 2, --12,
--      IOSTANDARD => "DEFAULT",
--      SLEW => "SLOW")
--   port map (
--      O => o_FMC2_SC_FROMMALTA_P,     -- Buffer output (connect directly to top-level port)
--      I => m_fromMalta      -- Buffer input 
--   );

----   OBUFDS_FMC2TOMALTA : OBUFDS
----    generic map (
----       IOSTANDARD => "DEFAULT",  -- Specify the output I/O standard
----       SLEW       => "SLOW")     -- Specify the output slew rate
----    port map (
----       O  => o_FMC2_SC_TOMALTA_P,   -- Diff_p output (connect directly to top-level port)
----       OB => o_FMC2_SC_TOMALTA_N,   -- Diff_n output (connect directly to top-level port)
----       I  => m_toMalta      -- Buffer input 
----    );

--      OBUF_FMC2TOMALTA : OBUF
--   generic map (
--      --DRIVE => 2, --12,
--      IOSTANDARD => "DEFAULT",
--      SLEW => "SLOW")
--   port map (
--      O => o_FMC2_SC_TOMALTA_P,     -- Buffer output (connect directly to top-level port)
--      I => i_toMalta      -- Buffer input 
--   );
   
--     m_Pixels_monitor(0)<= i_DATA_PIN(3);
--     m_Pixels_monitor(1)<= i_DATA_PIN(2);
     
--  FMC2_PINS : for I in 0 to 1 generate
      
--   OBUFDS_datapin_FMC2 : OBUFDS
--    generic map (
--       IOSTANDARD => "LVDS", -- Specify the output I/O standard
--       SLEW       => "FAST")          -- Specify the output slew rate
--    port map (
--       O  => o_FMC2_PIN_P(I),   -- Diff_p output (connect directly to top-level port)
--       OB => o_FMC2_PIN_N(I),   -- Diff_n output (connect directly to top-level port)
--       I  => m_Pixels_monitor(I)        --m_clk_2Hz --m_clk_10MHz        -- Buffer input 
--    );
    
--   end generate FMC2_PINS;
   

---------------------------------------------------------------------------------------------------------------------------------------------------      

end Behavioral;
