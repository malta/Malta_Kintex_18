-------------------------------------------------------------------------
-- Cleaning up warnings (Lluis) 22-02-2018
--  Critical warnings:
--    - Multi driver pin: Comment out line 80 in ao/SD_PACK.oversampling/Inst_IDELAY_CFG, net m_enable
--    - 'set property' expects at least one object: comment out lines 4 and 5 in constraint file "gig_ethernet_pcs_pma_0_board.xdc" -- This needs to be discussed and checked
--       The file is in the path: MaltaReadout_9/MaltaReadout_9.srcs/sources_1/ip/gig_ethernet_pcs_pma_0
--  Warnings:
--    - 'clk_out_3' is missing in component declaration: simply removed it from Clk_Provider2 wizard
--    - null port 'oob_in/out' ignored: add those ports in ipbus_top line 119, 120 and connect them to 'open'
--    - net ipb_out.ipb_addr does not have driver: add this line "ipb_out.ipb_addr <= (others => '0');" in the ipbus_fabric.vhd module, line 172.
--    - o_status[10 to 31] is not connected: initialize it in oversampling_module, line 26
--    - tying undriven pin ... : initialize signals:  m_out in AOTop, line 101. m_fifoW_out/in in Top_Readout lines 227 and 228. Missing m_busy and m_Re 
--    - oversampling_module has unconnected port ... : 
--    - tying undriven pin to '0': in Top_Readout, around line 568, added: m_busy(19 downto 11) <= (others => '0');
-------------------------------------------------------------------------

-------------------------------------------------------------------------
-- MALTA Readout Top File
-- 
-- Includes 
--  SlowControl
--  Asynch Oversampling
--  IPBus
-- 
-- Carlos.Solans@cern.ch
-- Enrico.Junior.Schioppa@cern.ch
-- Valerio.Dao@cern.ch
-- December 2017
------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.readout_constants.all;


entity Top_Readout is
 Port (
  SYSCLK_P          : in std_logic;
  SYSCLK_N          : in std_logic;
  
  i_USER_SMA_CLOCK_P : in std_logic;
  i_USER_SMA_CLOCK_N : in std_logic;
  
  i_GPIO_SW_N         : in std_logic;
  i_GPIO_SW_S         : in std_logic;
  
  --official input FMC
  i_FMC_PIN_P       : in std_logic_vector(NPINS-1 downto 0); 
  i_FMC_PIN_N       : in std_logic_vector(NPINS-1 downto 0);
  
  -- external clock
--  i_SMA_EXT_CLK_P   : in std_logic;
--  i_SMA_EXT_CLK_N   : in std_logic;
  
  --SC pins
  o_TRIG_TOMALTA_P   : out std_logic;
  o_TRIG_TOMALTA_N   : out std_logic;
  o_VPULSE_TOMALTA_P : out std_logic;
  o_VPULSE_TOMALTA_N : out std_logic;
  o_FMC_TOMALTA_P    : out std_logic;
  o_FMC_TOMALTA_N    : out std_logic;
  i_FMC_FROMMALTA_P  : in std_logic;
  i_FMC_FROMMALTA_N  : in std_logic;
  o_CLK_TOMALTA_P    : out std_logic;
  o_CLK_TOMALTA_N    : out std_logic;
  o_RST_TOMALTA      : out std_logic;
  
  --for IPBUS
  SGMIICLK_Q0_P     : in  std_logic;
  SGMIICLK_Q0_N     : in  std_logic;
  SGMII_TX_P        : out std_logic;
  SGMII_TX_N        : out std_logic;
  SGMII_RX_P        : in  std_logic;
  SGMII_RX_N        : in  std_logic;
  PHY_RESET         : out std_logic; 
  
  --for handling MALTA
  o_VREF           : out std_logic;
  o_PWR_DIG        : out std_logic;
  o_PWR_ANA        : out std_logic;
  o_PWR_LVDS       : out std_logic;
  o_PWR_DAC        : out std_logic;  
  i_MON_PWR_DIG    : in std_logic;
  i_MON_PWR_ANA    : in std_logic;
  i_MON_PWR_LVDS   : in std_logic;
  i_MON_PWR_DAC    : in std_logic;      
  
  --for monitoring
  i_SMA_user_P      : in std_logic;  --was out
  o_SMA_user_N      : out std_logic;
  i_swi             : in  std_logic_vector(2 downto 0);
  --o_trigger_P       : out std_logic;     
  --o_trigger_N       : out std_logic;     
  --o_FMC_LED_P       : out std_logic_vector(7 downto 0);
  --o_FMC_LED_N       : out std_logic_vector(7 downto 0);
  o_LED             : out std_logic_vector(7 downto 0);
  
  --external clock
  i_EXT_CLK_P       : in std_logic;
  i_EXT_CLK_N       : in std_logic;
  
  --FMC2 LEDs
  o_FMC2_LED        : out std_logic_vector (9 downto 0);
  --o_FMC2_SMA        : out std_logic_vector (9 downto 0)
  o_FMC2_SMA        : out std_logic_vector (9 downto 0)
  
 );
 
end Top_Readout;

architecture Main of Top_Readout is

--first all the IPs
 component Clk_Provider is 
 port(
  clk_out1  : out std_logic  :='0';
  clk_out2  : out std_logic  :='0';
  clk_out3  : out std_logic  :='0';
  clk_out4  : out std_logic  :='0';
--  clk_out5  : out std_logic  :='0';
--  clk_out6  : out std_logic  :='0';
  reset     : in  std_logic  :='0';
  locked    : out std_logic  :='0';
  clk_in1   : in  std_logic  :='0'
--  clk_in1_p : in  std_logic  :='0';
--  clk_in1_n : in  std_logic  :='0' 
 );
 end component Clk_Provider;

--first all the IPs
 component Clk_Provider2 is 
 port(
  clk_out1  : out std_logic  :='0';
  clk_out2  : out std_logic  :='0';
  reset     : in  std_logic  :='0';
  locked    : out std_logic  :='0';
  clk_in1   : in  std_logic  :='0'
--  clk_in1_p : in  std_logic  :='0';
--  clk_in1_n : in  std_logic  :='0' 010011101100

 );
 end component Clk_Provider2;
 
 component Clk_Provider3 is 
 port(
  clk_out1  : out std_logic  :='0';
  clk_out2  : out std_logic  :='0';
  clk_out3  : out std_logic  :='0';
  reset     : in  std_logic  :='0';
  locked    : out std_logic  :='0';
  clk_in1_p : in  std_logic  :='0';
  clk_in1_n : in  std_logic  :='0'
 );
 end component Clk_Provider3;

-- Dummy, Status, Reset and Busy
 signal m_dummy   : std_logic_vector(31 downto 0) := X"00000012";
 signal m_dummy2  : std_logic_vector(31 downto 0) := X"00000666";
 signal m_status  : std_logic_vector(31 downto 0) := X"20180406";
 signal m_reset   : std_logic_vector(31 downto 0) := X"00000000";
 signal m_resetSt : natural range 0 to 10 := 0;
 signal m_resetIP : std_logic :='0';
 signal m_busy    : std_logic_vector(31 downto 0) := (others => '0');
 signal m_sys_rst_inverted : std_logic := '0';
 signal m_sys_rst_TMP      : std_logic := '0';
 signal m_sys_rst_TMP2     : std_logic := '1';
 signal m_enable_all       : std_logic := '0';
 signal m_testCesar        : std_logic_vector (NPINS-1 downto 0) := (others =>'0');--: std_logic := '0'; 
 signal m_counter         : integer := 40;
 signal m_testCesar_del    : std_logic_vector (2 downto 0) := (others =>'0');--: std_logic := '0'; 
 signal m_finalDebug       : std_logic := '0'; 
 
-- only main clks defined here
 signal m_clk_500MHz     : std_logic;
 signal m_clk_500MHz_90  : std_logic;
 signal m_clk_200MHz_idel: std_logic;
 signal m_clk_200MHz_ip  : std_logic;
 signal m_clk_40MHz      : std_logic;     
 signal m_clk_10MHz      : std_logic;
 signal m_clk_Slow       : std_logic;     
 signal m_clk_IPbus      : std_logic;
 signal m_clk_IPbus2     : std_logic;
 signal m_extClk_320MHz  : std_logic;
 signal m_extClk_b_320MHz: std_logic;
 signal m_extClk_40MHz   : std_logic;

-- IPbus variables
 signal m_sys_rst        : std_logic := '0';
 signal m_ipbw           : ipb_wbus_array(99 downto 0);
 signal m_ipbr           : ipb_rbus_array(99 downto 0);
 signal m_ipbw2          : ipb_wbus_array(99 downto 0);
 signal m_ipbr2          : ipb_rbus_array(99 downto 0);
 
-- for monitoring
 signal m_led            : std_logic_vector( 7 downto 0) := (others => '0');
 signal m_led_out        : std_logic_vector( 7 downto 0) := (others => '0');
 
 --for handling MALTA
 signal m_PWR_DIG  : std_logic := '0';
 signal m_PWR_ANA  : std_logic := '0';
 signal m_PWR_LVDS : std_logic := '0';
 signal m_PWR_DAC  : std_logic := '0';  
 signal m_MON_DIG  : std_logic := '0';
 signal m_MON_ANA  : std_logic := '0';
 signal m_MON_LVDS : std_logic := '0';
 signal m_MON_DAC  : std_logic := '0';  

-- signals for trigger
 signal m_L1Ainput_ff_p     : std_logic := '0';
 signal m_L1Ainput_ff_n     : std_logic := '0';
 signal m_L1Ainput_ff_n_ff  : std_logic := '0';
 signal trig                : std_logic := '0';
 signal trig_del            : std_logic_vector(7 downto 0) := (others => '0');
  
-- for communicatoin with dummy sender
 signal m_Dummy_Command  : std_logic_vector(IPBUS_WIDTH-1 downto 0) := (others => '0');  --general purpose communication string
 signal m_Delay_Command  : std_logic_vector(IPBUS_WIDTH-1 downto 0) := (others => '0');  
 signal m_Delay_Counter  : std_logic_vector(5 downto 0) := (others => '0');  
 signal m_Dummy_Push     : std_logic := '0';
 signal m_Dummy_Reset    : std_logic := '0';
 signal m_Dummy_Swi_1    : std_logic := '0'; 
 signal m_Dummy_Swi_2    : std_logic := '0'; 
 signal m_Dummy_Swi_3    : std_logic := '0'; 
 signal m_Dummy_Swi_4    : std_logic := '0';
 signal m_Dummy_Swi_5    : std_logic := '0';  

-- this is for AO module needs
 signal m_AOreset        : std_logic := '0'; 
 signal m_AOfifo1_empty  : std_logic := '1';
 signal m_AOfifo2_empty  : std_logic := '1';
 signal m_AOfifoM_empty  : std_logic := '1';
 signal m_AOfifo1_almost : std_logic := '0';
 signal m_AOfifo1_half   : std_logic := '0'; ---prog full
 signal m_AOfifo2_almost : std_logic := '0';
 signal m_AOfifo2_half   : std_logic := '0'; ---prog empty
 signal m_AOfifo2_rdCtr  : std_logic_vector(15 downto 0) := (others => '0');
 signal m_AOfifoM_almost : std_logic := '0';

-- Slow Control 
 signal m_fromMALTA      : std_logic := '1'; -- must be '1' by Slow Control protocol
 signal m_toMALTA        : std_logic := '1'; -- must be '1' by Slow Control protocol   
 --signal m_toMALTA_inv    : std_logic := '1'; -- tmp for debugging   
 signal m_trig_toMalta   : std_logic := '0';
 signal m_vpulse_toMalta : std_logic := '0';
 -- Load  17/03/2021 Added Leyre so review me!!!
 --signal m_load           : std_logic := '0'; -- It should be asserted to 1 only after 4322 periods of 10 MHz clock

 signal m_fifoR_rdclk: std_logic;
 signal m_fifoR_wrclk: std_logic;
 signal m_fifoR_out  : std_logic_vector(31 downto 0);
 signal m_fifoR_in   : std_logic_vector(31 downto 0);
 signal m_fifoR_rden : std_logic := '0';
 signal m_fifoR_wren : std_logic := '0';
 signal m_fifoR_empty: std_logic := '1';
 signal m_fifoR_full : std_logic := '0';
 signal m_fifoR_size : std_logic_vector(31 downto 0);
 
 signal m_fifoW_rdclk: std_logic;
 signal m_fifoW_wrclk: std_logic;
 signal m_fifoW_out  : std_logic_vector(31 downto 0) := (others => '0'); 
 signal m_fifoW_in   : std_logic_vector(31 downto 0) := (others => '0');
 signal m_fifoW_rden : std_logic := '0';
 signal m_fifoW_wren : std_logic := '0';
 signal m_fifoW_empty: std_logic := '1';
 signal m_fifoW_full : std_logic := '0';
 signal m_fifoW_size : std_logic_vector(31 downto 0); 

 signal m_trigger  : std_logic:='0';
 signal m_TMPsysCLK: std_logic:='0';
  
 signal m_MaltaPushReset    : std_logic:='0';
 signal m_MaltaSC_ClkEnable : std_logic:='0';
 signal m_MaltaHighImpedance: std_logic:='1';
 signal m_ClkToMalta        : std_logic:='0';
 signal m_clk_10MHz_Delay1      : std_logic;
 signal m_clk_10MHz_Delay2      : std_logic;
 
 signal m_L1Atmp      : std_logic:='0';
 signal m_L1A         : std_logic:='0';
 signal m_L1AtoPico   : std_logic:='0';
 signal m_L1AtoMalta  : std_logic:='0';
 signal m_L1Afull     : std_logic:='0';
 signal m_L1Ainput    : std_logic:='0';
 signal m_L1A_selector: std_logic:='0';
 signal m_L1Adebug        : std_logic:='0';


 signal m_RO_disable            : std_logic:='0';
 signal m_RO_disable_HalfColumn : std_logic:='0';
 signal m_RO_disable_HalfRow    : std_logic:='0';
 
 signal m_DataLinesIn : std_logic_vector(40 downto 0) := (others => '0');
 
 signal m_noisyPixels_monitor : std_logic_vector (1 downto 0);
 
 -- Bojan counter
 signal m_counter_out     : std_logic_vector(TRIG_ID_SIZE-1 downto 0)  := (others => '0');
 signal m_slowCounter_dbg : std_logic_vector(2 downto 0)  := (others => '0');
 signal m_maxCounterVal   : std_logic_vector (8 downto 0);
 
 signal m_fastSignal      : std_logic:='0';
 signal m_fastSignalLong  : std_logic:='0';
 signal m_counterFast     : natural range 0 to 31 :=0;
 signal m_fastEnable      : std_logic:='0';
 
 signal m_debug1          : std_logic:='0';
 signal m_debug2          : std_logic:='0';
 signal m_clipper_deb1    : std_logic:='0';
 signal m_clipper_deb2    : std_logic:='0';
 
 -- pico TDC test
 signal m_toTDC           : std_logic_vector(3 downto 0):= (others => '0');
 
 -- IPB FIFO word count
 signal m_fifo2_wordCount : std_logic_vector(31 downto 0):= (others => '0');
 
begin

---------------------------------------------------------------------------------------------------------------------------------------------------
-- System reset: North button or reset register bit 0
 m_sys_rst <= i_GPIO_SW_N or m_reset(0);
 PHY_RESET <= not m_sys_rst ;
 m_sys_rst_TMP  <= m_sys_rst_TMP2 and (i_GPIO_SW_S or m_MaltaPushReset);
 --m_sys_rst_TMP  <= m_sys_rst_TMP2 and (i_GPIO_SW_S);
 --m_MaltaHighImpedance <= not m_MaltaPushReset;
 m_sys_rst_inverted <= not m_sys_rst_TMP;

 -- input clk
 inL: IBUFGDS
 port map( O=>m_TMPsysCLK, I=>SYSCLK_P, IB=>SYSCLK_N);
 
 OBUF_VREF: OBUF
 generic map (
   DRIVE => 12,
   IOSTANDARD => "DEFAULT",
   SLEW => "SLOW")
 port map (
   O => o_VREF,
   I => '1'
 );
 
  -- external clk
-- inEXTCLK: IBUFGDS
-- port map( O=>m_clk_10MHz, I=>i_EXT_CLK_P, IB=>i_EXT_CLK_N);
---------------------------------------------------------------------------------------------------------------------------------------------------
-- main clk object
 mainClk1 : Clk_Provider
 port map(
  reset=>'0', locked=>open,
  clk_out1  => m_clk_500MHz, 
  clk_out2  => m_clk_500MHz_90,  
  clk_out3  => m_clk_40MHz, 
  clk_out4  => m_clk_10MHz, 
  clk_in1   => m_TMPsysCLK
 );

 mainClk2 : Clk_Provider2
 port map(
  reset=>'0', locked=>open,
  clk_out1  => m_clk_200MHz_idel, 
  clk_out2  => m_clk_200MHz_ip,
  clk_in1   => m_TMPsysCLK
 );
 
-- mainClk3 : Clk_Provider3
-- port map(
--  reset=>'0', locked=>open,
--  clk_out2   => m_extClk_320MHz, 
--  clk_out3   => m_extClk_b_320MHz,
--  clk_out1   => m_extClk_40MHz,
--  clk_in1_p  => i_USER_SMA_CLOCK_P,
--  clk_in1_n  => i_USER_SMA_CLOCK_N
-- );

---------------------------------------------------------------------------------------------------------------------------------------------------
-- IPBUS top
 ipbus: entity work.ipbus_top
 generic map(NSLV => NSLAVES, ALT => 1  )
 port map(
  ipb_to_slaves   => m_ipbw,
  ipb_from_slaves => m_ipbr,
  i_clk200        => m_clk_200MHz_ip,
  o_clk1hz        => open, --m_led(0),
  o_clk_locked    => open, --m_led(0),
  o_eth_locked    => open, --m_led(7),
  i_addrSwi       => i_swi,
  o_ipb_clk       => m_clk_IPbus,
  i_sys_rst       => m_sys_rst,
  o_ipb_rx_led    => open, --m_led(2),
  o_ipb_tx_led    => open, --m_led(4),
  i_gt_clkp       => SGMIICLK_Q0_P,
  i_gt_clkn       => SGMIICLK_Q0_N,
  o_gt_txp        => SGMII_TX_P,
  o_gt_txn        => SGMII_TX_N,
  i_gt_rxp        => SGMII_RX_P,
  i_gt_rxn        => SGMII_RX_N
 );
 
 ipbus2: entity work.ipbus_top
 generic map(NSLV => NSLAVES,  ALT => 2 )
 port map(
  ipb_to_slaves   => m_ipbw2,
  ipb_from_slaves => m_ipbr2,
  i_clk200        => m_clk_200MHz_ip,
  o_clk1hz        => open, --m_led(0),
  o_clk_locked    => open, --m_led(0),
  o_eth_locked    => open, --m_led(7),
  i_addrSwi       => i_swi,
  o_ipb_clk       => m_clk_IPbus2,
  i_sys_rst       => m_sys_rst,
  o_ipb_rx_led    => open, --m_led(2),
  o_ipb_tx_led    => open, --m_led(4),
  i_gt_clkp       => SGMIICLK_Q0_P,
  i_gt_clkn       => SGMIICLK_Q0_N,
  o_gt_txp        => open, --SGMII_TX_P,
  o_gt_txn        => open, --SGMII_TX_N,
  i_gt_rxp        => SGMII_RX_P,
  i_gt_rxn        => SGMII_RX_N
 );

 -- Register 0: Version (R)
 dummy: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_VERSION,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_VERSION),
  o_ipbr     => m_ipbr(IPBADDR_VERSION),
  c_readonly => '1',
  i_default  => m_dummy,
  o_signal   => open
 );
 
 -- Register 0: Version (R)
 dummy2: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_VERSION,32)),
  i_clk      => m_clk_IPbus2,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw2(IPBADDR_VERSION),
  o_ipbr     => m_ipbr2(IPBADDR_VERSION),
  c_readonly => '1',
  i_default  => m_dummy2,
  o_signal   => open
 );
  
 -- Register 1: Status (R)
 status: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_STATUS,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_STATUS),
  o_ipbr     => m_ipbr(IPBADDR_STATUS),
  c_readonly => '1',
  i_default  => m_status,
  o_signal   => open
 );
  
 -- Register 2: Reset (W)
 reset: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_RESET,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_RESET),
  o_ipbr     => m_ipbr(IPBADDR_RESET),
  c_readonly => '0',
  i_default  => X"00000000",
  o_signal   => m_reset
 );
  
 
 --Need a 2 state approach
 process(m_clk_10MHz)
 begin
 if rising_edge(m_clk_10MHz) then
  if m_reset(0)='1' and m_reset(1)='0' and m_resetSt=0 then --enable when the signal rise
   m_resetIP <= '1';
   m_resetSt <= 1;
  elsif m_resetSt=1 then
   --m_resetIP <= '0';
   m_resetSt <= 2;
  elsif m_resetSt>=2 and m_resetSt<10  then
   m_resetSt <= m_resetSt+1;
  elsif m_resetSt=10 then --and m_reset(1)='1' and m_reset(0)='0' then   
   m_resetIP <= '0'; 
   m_resetSt <= m_resetSt+1;
   --m_resetSt <= 0;
  end if;
  
  if m_reset(0)='0' and m_reset(1)='1' then
   m_resetSt <= 0;
  end if;
  
 end if;
 end process;
 
 -- Register 3: Busy (R)
 busy: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_BUSY,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_BUSY),
  o_ipbr     => m_ipbr(IPBADDR_BUSY),
  c_readonly => '1',
  i_default  => m_busy,
  o_signal   => open
 );
  
-----------------------------------------------------------------------------------------------------------------------------------------------------
---- SLOW CONTROL: AKA TREX
 TRex: entity work.slow_control
 port map(
 -- clock
  i_clk10       => m_clk_10MHz,
  i_fifoW_clk32 => m_fifoW_wrclk,
  -- reading
  i_fifoR_clk32 => m_clk_IPbus,
  i_fifoR_rden  => m_fifoR_rden,
  o_fifoR_empty => m_fifoR_empty,
  o_fifoR_full  => m_fifoR_full,  
  o_fifoR_out   => m_fifoR_out,
  i_fromMALTA   => m_fromMALTA,
  -- writing  
  i_fifoW_wren  => m_fifoW_wren,
  o_fifoW_rden  => open,
  i_fifoW_in    => m_fifoW_in,
  o_fifoW_empty => m_fifoW_empty,
  o_fifoW_full  => m_fifoW_full,  
  o_serializer_start => open,
  o_serializer_busy  => open,                   
  o_toMALTA     => m_toMALTA,  
  -- reset
  i_reset       => m_AOReset --m_sys_rst,
  -- Load  17/03/2021 Added Leyre so review me!!!
  --i_load        => m_load;  
 );
 --m_fromMALTA <= m_toMALTA; -- SHORTENING SC from ENrico

 --m_ClkToMalta <= not (m_clk_10MHz and m_MaltaSC_ClkEnable);
 m_ClkToMalta <= m_clk_10MHz and m_MaltaSC_ClkEnable; --inverted clk to change the phase -- not inverted now (8/03/18)
     
 ----------------------------------------------------------------------------------------------------------------------------
 -- Register 4: SlowControl FIFO_R
 slowcontrol_r: entity work.ipbus_fifo
 generic map(
  c_ok_in_msb => '1'
 )
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_SCFIFO_R,32)), 
  i_clk      => m_clk_IPbus,
  i_ipbw     => m_ipbw(IPBADDR_SCFIFO_R),
  o_ipbr     => m_ipbr(IPBADDR_SCFIFO_R),
  c_readonly => '1',
  i_empty    => m_fifoR_empty,
  i_full     => m_fifoR_full, -- useless
  i_rddata   => m_fifoR_out,
  o_rdclk    => m_fifoR_rdclk,
  o_rdenable => m_fifoR_rden,
  o_wrdata   => open, -- useless
  o_wrclk    => open, -- useless
  o_wrenable => open -- useless
 );
      
 -- Register 5: Slow Control FIFO_W
 slowcontrol_w: entity work.ipbus_fifo
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_SCFIFO_W,32)),
  i_clk      => m_clk_IPbus,
  i_ipbw     => m_ipbw(IPBADDR_SCFIFO_W),
  o_ipbr     => m_ipbr(IPBADDR_SCFIFO_W),
  c_readonly => '0',
  i_empty    => m_fifoW_empty, -- useless
  i_full     => m_fifoW_full,
  i_rddata   => m_fifoW_out,   -- useless
  o_rdclk    => m_fifoW_rdclk, -- useless
  o_rdenable => m_fifoW_rden,  -- uesless
  o_wrdata   => m_fifoW_in,
  o_wrclk    => m_fifoW_wrclk,
  o_wrenable => m_fifoW_wren
 );

 ---------------------------------------------------------------------------------------------------------------------------------------------------
 m_L1Ainput  <= ( (not m_L1A_selector) and m_Dummy_Command(19)) or (m_L1A_selector and i_SMA_user_P);     --THIS IS FOR HSIO
 --m_L1Ainput  <= ( (not m_L1A_selector) and m_Dummy_Command(19)) or (m_L1A_selector and not i_SMA_user_P);   --THIS IS FOR SCINTILLATOR
 ---------m_L1Atmp <= (not i_SMA_user_P);
 
 clipperToPico: entity work.L1AClipper(picoTDC)
 --clipperToPico: entity work.L1AClipper(picoTDCold)
 port map( 
  i_clk_fast    => m_clk_500MHz,
  i_clk_slow    => m_Clk_40MHz,--m_extClk_40MHz, 
  i_sig         => m_L1A, --m_Dummy_Command(19),
  o_sig         => m_L1AtoPico,
  o_debug1      => m_clipper_deb1,
  o_debug2      => m_clipper_deb2
 );
 
 clipperToMalta: entity work.L1AClipper(maltaAO)
 port map( 
  i_clk_fast    => m_clk_500MHz,
  i_clk_slow    => m_Clk_40MHz,--m_extClk_40MHz, 
  --i_clk_slow    => m_extClk_40MHz, 
  i_sig         => m_L1Ainput, --m_Dummy_Command(19),
  o_sig         => m_L1A
 );
 
 clipperToMaltadebug: entity work.L1AClipper(maltaAOdebug)
 port map( 
  i_clk_fast    => m_clk_500MHz,
  i_clk_slow    => m_Clk_40MHz,--m_extClk_40MHz, 
  --i_clk_slow    => m_extClk_40MHz, 
  i_sig         => m_L1Ainput, --m_Dummy_Command(19),
  o_sig         => m_L1Adebug
 );

-- process (m_clk_40MHz) is begin
 
--        if rising_edge(m_clk_40MHz) then
--                  m_L1Ainput_ff_p <= m_L1Ainput;     
--                  m_L1Ainput_ff_n <= not m_L1Ainput;
--        end if;
        
-- end process;
 
-- process (m_clk_40MHz) is begin
  
--         if rising_edge(m_clk_40MHz) then
--                   m_L1Ainput_ff_n_ff <= m_L1Ainput_ff_n;
--         end if;     
  
--  end process; 
  
--  trig <= m_L1Ainput_ff_n_ff and m_L1Ainput_ff_p; 
  
--   process (m_clk_40MHz) is begin
   
--          if rising_edge(m_clk_40MHz) then
--                    trig_del(0) <= trig;
--                    trig_del(7 downto 1) <= trig_del(6 downto 0);
--          end if;     
   
--   end process; 
  
  
--  AO module : Registers 6 and 7
  ao: entity work.AOTop
  port map(  
   i_clk_500MHz     => m_clk_500MHz, 
   i_clk_500MHz_90  => m_clk_500MHz_90,  
   i_clk_Idel       => m_clk_200MHz_idel,  
   i_clk_IPb        => m_clk_IPbus,
   i_clk_40MHz      => m_clk_40MHz,
   i_ResetFifo      => m_AOReset,
   i_ipbw           => m_ipbw,
   i_ipbr           => m_ipbr,
   i_PIN_P          => i_FMC_PIN_P, 
   i_PIN_N          => i_FMC_PIN_N,
   o_test           => m_testCesar,
   o_debug1         => m_debug1,
   o_debug2         => m_debug2,
   o_refBeforeFifo  => m_FinalDebug,
   o_fastSignal     => m_fastSignal,
   --o_test2         => m_testCesar2,
   o_fifo1_empty    => m_AOfifo1_empty,
   o_fifo2_empty    => m_AOfifo2_empty,
   o_fifoM_empty    => m_AOfifoM_empty,
   o_fifo1_Full     => m_AOfifo1_almost,
   o_fifo1_HalfFull => m_AOfifo1_half,
   o_fifo2_ProgEmpty=> m_AOfifo2_half,
   o_fifo2_rdCount  => m_AOfifo2_rdCtr,
   o_fifo2_Full     => m_AOfifo2_almost,
   o_fifoM_Full     => m_AOfifoM_almost,
   -- bojan slow counter for trigger ID
   i_trigID_reset   => m_Dummy_Command(16), 
   i_trigID_hold    => m_Dummy_Command(17), 
   i_trigID_mode    => m_Dummy_Command(18), 
   i_trigID_trig    => m_L1Afull, 
   o_trigID_out     => m_counter_out,
   o_count_dbg      => m_slowCounter_dbg,
   i_Delay_Counter  => m_Delay_Counter,
   i_maxCounterVal  => m_maxCounterVal,
   i_RO_disable            => m_RO_disable,
   i_RO_disable_HalfColumn => m_RO_disable_HalfColumn,
   i_RO_disable_HalfRow    => m_RO_disable_HalfRow
  );
  o_SMA_user_N <= m_fastSignalLong and m_fastEnable;

  m_busy(0)  <= m_AOfifo1_almost;
  m_busy(1)  <= m_AOfifo2_almost;
  m_busy(2)  <= m_AOfifoM_almost;
  m_busy(3)  <= not m_AOfifo1_empty;
  m_busy(4)  <= not m_AOfifo2_empty;
  m_busy(5)  <= not m_AOfifoM_empty;
  m_busy(6)  <= m_AOfifo1_half;
  m_busy(7)  <= not m_fifoR_empty;
  m_busy(8)  <= m_fifoR_full;
  m_busy(9)  <= not m_fifoW_empty;
  m_busy(10) <= m_fifoW_full;
  m_busy(11) <= m_AOfifo2_half;
  --m_busy(19 downto 11) <= (others => '0'); ??????
  --VD: movnig these from 20,21,22,23 to 11,12,13,14 to make room for the L1IDcounter
  m_busy(12) <= m_MON_DIG;
  m_busy(13) <= m_MON_ANA;
  m_busy(14) <= m_MON_LVDS;
  m_busy(15) <= m_MON_DAC;
  m_busy(31 downto 20) <= m_counter_out(17 downto 6);    -- Bojan
  
  --m_fastSignalLong <=m_fastSignal;
--  process (m_clk_500MHz) is begin 
--    if (rising_edge(m_clk_500MHz)) then
--      if m_counterFast=0 then
--        if m_fastSignal='1' then
--          m_counterFast <=  m_counterFast+1;
--          m_fastSignalLong <= '1';
--        end if;
--      elsif m_counterFast=1 then
--        m_counterFast <=  m_counterFast+1;
--      elsif m_counterFast=2 then
--        m_counterFast <=  m_counterFast+1;
--        m_fastSignalLong <= '0';
--      else 
--        m_counterFast <= 0;
--      end if; 
--    end if; 
--  end process;    
  process (m_clk_500MHz) is begin 
    if (rising_edge(m_clk_500MHz)) then
      if m_counterFast=0 then
        if m_fastSignal='1' then
          m_counterFast <=  m_counterFast+1;
          m_fastSignalLong <= '1';
        end if;
      elsif m_counterFast<2 then --was 4 length of signal to TLU 6.25 ns
        m_fastSignalLong <= '1';
        m_counterFast <=  m_counterFast+1;
--      elsif m_counterFast<13 then
--        m_counterFast <=  m_counterFast+1;    --15
--        m_fastSignalLong <= '0';
      else
        m_fastSignalLong <= '0'; 
        m_counterFast <= 0;
      end if; 
    end if; 
  end process; 
  
  m_L1Afull <= m_L1A;
  m_fastEnable <= m_Delay_Command(6);
  ---------------------------------o_SMA_user_N <= (m_fastSignalLong and m_fastEnable); --m_L1Afull; --m_FinalDebug; --m_testCesar(0); --m_AOfifo1_half;             
--  o_SMA_user_N <= m_L1Afull; --m_FinalDebug; --m_testCesar(0); --m_AOfifo1_half;             

  --Modification from Francesco
  -------------------------------------------------------------------------------------------------------------------------- 
    
--    m_toTDC(0)  <= m_testCesar(0) when m_RO_disable = '0';
--    m_toTDC(1)  <= m_testCesar(1) when m_RO_disable = '0';
--    m_toTDC(2)  <= m_testCesar(2) when m_RO_disable = '0';
--    m_toTDC(3)  <= m_testCesar(3) when m_RO_disable = '0';


 ------------------------------
  
--process(m_clk_40MHz)
--begin
--    if rising_edge(m_clk_40MHz) then
--      m_testCesar_del(0) <= m_testCesar(0);
--      m_testCesar_del(2 downto 1)<=m_testCesar_del(1 downto 0);
--    end if;
--end process;


    
--------------------------------

o_FMC2_SMA(0) <= m_testCesar(0);
o_FMC2_SMA(1) <= m_L1Adebug;



--o_FMC2_SMA(2) <= m_fastSignalLong and m_fastEnable;
--o_FMC2_SMA(3) <= m_debug1;
  
Pico_clk : OBUFDS
generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "FAST")    
port map (
    O  => o_FMC2_SMA(2),
    OB => o_FMC2_SMA(3),
    I  => m_clk_40MHz
);   
    
    
Pico_test0 : OBUFDS
generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "FAST")    
port map (
    O  => o_FMC2_SMA(4),
    OB => o_FMC2_SMA(5),
    I  => m_testCesar(0)
); 
            
Pico_test1 : OBUFDS
generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "FAST")    
port map (
    O  => o_FMC2_SMA(6),
    OB => o_FMC2_SMA(7),
    I  => m_L1AtoPico
);

Pico_test2 : OBUFDS
generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "FAST")    
port map (
    O  => o_FMC2_SMA(8),
    OB => o_FMC2_SMA(9),
    I  => m_L1Ainput
);    
       
 ---------------------------------------------------------------------------------------------------------------------------------------------------
 -- Delay control
 delayCommand: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_DELAY,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_DELAY),
  o_ipbr     => m_ipbr(IPBADDR_DELAY),
  c_readonly => '0',
  i_default  => m_Delay_Command, 
  o_signal   => m_Delay_Command
 );
 m_Delay_Counter <= m_Delay_Command(5 downto 0);
 m_maxCounterVal <= m_Delay_Command(18 downto 10);
 
 ---------------------------------------------------------------------------------------------------------------------------------------------------
 -- Delay control
 FIFO2_WordCount: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(AO_ADDR_WDCTR,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(AO_ADDR_WDCTR),
  o_ipbr     => m_ipbr(AO_ADDR_WDCTR),
  c_readonly => '1',
  i_default  => m_fifo2_wordCount, 
  o_signal   => open
 );
 m_fifo2_wordCount(31 downto 16) <= (others => '0');
 m_fifo2_wordCount(15 downto  0) <= m_AOfifo2_rdCtr;
 
 ---------------------------------------------------------------------------------------------------------------------------------------------------
 -- DummySender control
 slaveCommand: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_DUMCTR,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_DUMCTR),
  o_ipbr     => m_ipbr(IPBADDR_DUMCTR),
  c_readonly => '0',
  i_default  => m_Dummy_Command, 
  o_signal   => m_Dummy_Command
 );
 m_Dummy_Push   <= m_Dummy_Command(0);
 m_Dummy_Reset  <= m_Dummy_Command(1);
 m_AOReset      <= m_Dummy_Command(2) or m_sys_rst;
 m_Dummy_Swi_1  <= m_Dummy_Command(3);
 m_Dummy_Swi_2  <= m_Dummy_Command(4);
 m_Dummy_Swi_3  <= m_Dummy_Command(5);
 m_Dummy_Swi_4  <= m_Dummy_Command(6);
 m_Dummy_Swi_5  <= m_Dummy_Command(7);

 m_PWR_DIG  <= m_Dummy_Command(8);
 m_PWR_ANA  <= m_Dummy_Command(9);
 m_PWR_LVDS <= m_Dummy_Command(10);
 m_PWR_DAC  <= m_Dummy_Command(11);
 m_MaltaPushReset    <= m_Dummy_Command(12);
 m_vpulse_toMalta    <= m_Dummy_Command(20);
 m_MaltaSC_ClkEnable <= m_Dummy_Command(21);
 m_L1A_selector      <= m_Dummy_Command(18);
  
 m_RO_disable            <= m_Dummy_Command(29);
 m_RO_disable_HalfColumn <= m_Dummy_Command(30);
 m_RO_disable_HalfRow    <= m_Dummy_Command(31); 
 
---------------------------------------------------------------------------------------------------------------------------------------------------

 Monitoring: entity work.Malta_Monitoring
 port map ( 
  i_ClkToMalta          => m_ClkToMalta,
  i_toMalta             => m_toMalta,
  i_FMC_FROMMALTA_P     => i_FMC_FROMMALTA_P,
  i_FMC_FROMMALTA_N     => i_FMC_FROMMALTA_N,
  i_vpulse_toMalta      => m_vpulse_toMalta, 
  i_TRIG_TOMALTA        => m_clk_40MHz,
  i_RST_TOMALTA         => m_sys_rst_inverted,
  i_DATA_PIN            => m_testCesar,   
  o_RST_TOMALTA         => o_RST_TOMALTA,
  o_CLK_TOMALTA_P       => o_CLK_TOMALTA_P,
  o_CLK_TOMALTA_N       => o_CLK_TOMALTA_N,
  o_FMC_TOMALTA_P       => o_FMC_TOMALTA_P,
  o_FMC_TOMALTA_N       => o_FMC_TOMALTA_N,
  o_FROMMALTA           => m_fromMalta,
  o_VPULSE_TOMALTA_P    => o_VPULSE_TOMALTA_P,
  o_VPULSE_TOMALTA_N    => o_VPULSE_TOMALTA_N,
  o_TRIG_TOMALTA_P      => o_TRIG_TOMALTA_P,
  o_TRIG_TOMALTA_N      => o_TRIG_TOMALTA_N  
--    o_FMC2_SC_FROMMALTA_P => o_FMC2_SC_FROMMALTA_P,
--    o_FMC2_SC_TOMALTA_P   => o_FMC2_SC_TOMALTA_P,
--    o_FMC2_PIN_P          => o_FMC2_PIN_P,
--    o_FMC2_PIN_N          => o_FMC2_PIN_N      
 );

---------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 
 ---- output
 m_led_out(0) <= m_AOReset;    
 m_led_out(1) <= m_Dummy_Push; 
 m_led_out(2) <= m_Dummy_Reset; 
 m_led_out(3) <= m_Dummy_Swi_1;
 m_led_out(4) <= m_Dummy_Swi_2;
 m_led_out(5) <= m_Dummy_Swi_3;
 m_led_out(6) <= m_Dummy_Swi_4;
 m_led_out(7) <= m_Dummy_Swi_5;

 --DUMMY_SENDER_OUT : for P in 0 to 7 generate
 -- out_PIN : OBUFDS generic map( SLEW => "FAST") port map( I=>m_led_out(P), O=>o_FMC_LED_P(P), OB=>o_FMC_LED_N(P));
 --end GENERATE DUMMY_SENDER_OUT;
 
 m_led(0) <= m_L1A_selector; --m_AOfifo1_half; --m_sys_rst_inverted;
 m_led(1) <= m_Dummy_Command(16);
 m_led(2) <= i_swi(0);
 m_led(3) <= i_swi(1);
 m_led(4) <= i_swi(2);
 -------------m_led(7 downto 2) <= m_counter_out(11 downto 6);

 o_PWR_DIG  <= m_PWR_DIG;
 o_PWR_ANA  <= m_PWR_ANA;
 o_PWR_LVDS <= m_PWR_LVDS;
 o_PWR_DAC  <= m_PWR_DAC;
 m_MON_DIG  <= i_MON_PWR_DIG;
 m_MON_ANA  <= i_MON_PWR_ANA;
 m_MON_LVDS <= i_MON_PWR_LVDS;
 m_MON_DAC  <= i_MON_PWR_DAC;
 -- m_led(0) <= m_PWR_DIG;
 -- m_led(1) <= m_PWR_ANA;
 -- m_led(2) <= m_PWR_LVDS;
 -- m_led(3) <= m_PWR_DAC;  
 -- m_led(4) <= m_MON_DIG;
 -- m_led(5) <= m_MON_ANA;
 -- m_led(6) <= m_MON_LVDS;
 -- m_led(7) <= m_MON_DAC; 
 
 --putting the FIFO monitornig there:
 o_FMC2_LED(0)<=m_AOfifo1_empty;
 o_FMC2_LED(1)<=m_AOfifo2_empty;
 o_FMC2_LED(2)<=m_AOfifoM_empty;
 o_FMC2_LED(3)<=m_AOfifo1_almost;
 o_FMC2_LED(4)<=m_AOfifo2_almost;
 o_FMC2_LED(5)<=m_AOfifoM_almost;
 o_FMC2_LED(6)<=m_fifoR_empty;
 o_FMC2_LED(7)<=m_fifoR_full;
 o_FMC2_LED(8)<=m_fifoW_empty;
 o_FMC2_LED(9)<=m_fifoW_full;
 
 
-- o_FMC2_SMA(0)<=m_clk_500MHz;
-- o_FMC2_SMA(1)<=m_clk_40MHz;
-- o_FMC2_SMA(2)<=m_clk_10MHz; 
-- o_FMC2_SMA(3)<=m_Dummy_Command(19);
  
-- o_FMC2_SMA(0)<=m_FinalDebug; --m_L1Afull; --m_Dummy_Command(19);
-- o_FMC2_SMA(1)<='0';--i_SMA_user_P;  --m_testCesar(0);
 
-- o_FMC2_SMA(2)<=m_debug1; --'0';--m_testCesar(32); -----1 
-- o_FMC2_SMA(3)<='0'; --i_SMA_user_P; --m_testCesar(2);
-- o_FMC2_SMA(4)<=m_debug2;--m_testCesar(3);
-- o_FMC2_SMA(5)<='0';--m_testCesar(4);
-- o_FMC2_SMA(6)<='0';--m_testCesar(5);
-- o_FMC2_SMA(7)<='0';--m_testCesar(9);
-- o_FMC2_SMA(8)<=m_fastSignalLong;--m_testCesar(10);
-- o_FMC2_SMA(9)<='0';--m_testCesar(11);
 
 o_led <= m_led;
 
end Main;
