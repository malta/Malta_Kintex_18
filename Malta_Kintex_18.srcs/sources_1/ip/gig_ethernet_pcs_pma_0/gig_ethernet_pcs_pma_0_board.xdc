#--------------------Physical Constraints-----------------
set_property LOC GTXE2_CHANNEL_X0Y9 [get_cells -hi -regexp {.*transceiver_inst/gtwizard_inst/.*GTWIZARD_i/gt.e2_i}] 

set_property BOARD_PIN {SGMIICLK_Q0_P} [get_ports gtrefclk_p]
set_property BOARD_PIN {SGMIICLK_Q0_N} [get_ports gtrefclk_n]

