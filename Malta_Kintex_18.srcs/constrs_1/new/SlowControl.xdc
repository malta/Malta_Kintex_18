
set_property PACKAGE_PIN G14 [get_ports o_PWR_DIG] 
set_property PACKAGE_PIN F15 [get_ports o_PWR_ANA] 
set_property PACKAGE_PIN E16 [get_ports o_PWR_LVDS] 

#bOARDv3
set_property PACKAGE_PIN C19 [get_ports o_PWR_DAC] 
#

#Boardv2
#set_property PACKAGE_PIN G29 [get_ports o_PWR_DAC]
#

set_property PACKAGE_PIN D13 [get_ports i_MON_PWR_DIG] 
set_property PACKAGE_PIN E19 [get_ports i_MON_PWR_ANA] 
set_property PACKAGE_PIN D19 [get_ports i_MON_PWR_LVDS] 
set_property PACKAGE_PIN D12 [get_ports i_MON_PWR_DAC] 
set_property IOSTANDARD LVCMOS25 [get_ports o_PWR_DIG]
set_property IOSTANDARD LVCMOS25 [get_ports o_PWR_ANA]
set_property IOSTANDARD LVCMOS25 [get_ports o_PWR_LVDS]
set_property IOSTANDARD LVCMOS25 [get_ports o_PWR_DAC]
set_property IOSTANDARD LVCMOS25 [get_ports i_MON_PWR_DIG]
set_property IOSTANDARD LVCMOS25 [get_ports i_MON_PWR_ANA]
set_property IOSTANDARD LVCMOS25 [get_ports i_MON_PWR_LVDS]
set_property IOSTANDARD LVCMOS25 [get_ports i_MON_PWR_DAC]


set_property PACKAGE_PIN H14 [get_ports o_RST_TOMALTA] 
set_property IOSTANDARD LVCMOS25 [get_ports o_RST_TOMALTA]


#Bob board V3
#P32/LA33/U31/T31
set_property PACKAGE_PIN H21 [get_ports i_FMC_FROMMALTA_P] 
set_property PACKAGE_PIN H22 [get_ports i_FMC_FROMMALTA_N] 
##

#Bob board v2
#set_property PACKAGE_PIN P21 [get_ports i_FMC_FROMMALTA_P]
#set_property PACKAGE_PIN N21 [get_ports i_FMC_FROMMALTA_N]
##

set_property PACKAGE_PIN B18 [get_ports o_FMC_TOMALTA_P] 
set_property PACKAGE_PIN A18 [get_ports o_FMC_TOMALTA_N] 
set_property PACKAGE_PIN F21 [get_ports o_CLK_TOMALTA_P] 
set_property PACKAGE_PIN E21 [get_ports o_CLK_TOMALTA_N] 

#BoardV2
#set_property PACKAGE_PIN H26 [get_ports o_TRIG_TOMALTA_N] 
#set_property PACKAGE_PIN H25 [get_ports o_TRIG_TOMALTA_P]
#

#BoardV3
set_property PACKAGE_PIN F17 [get_ports o_TRIG_TOMALTA_N] 
set_property PACKAGE_PIN G17 [get_ports o_TRIG_TOMALTA_P] 
#

set_property PACKAGE_PIN A22 [get_ports o_VPULSE_TOMALTA_N] 
set_property PACKAGE_PIN B22 [get_ports o_VPULSE_TOMALTA_P] 
set_property IOSTANDARD LVDS_25 [get_ports o_FMC_TOMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports o_FMC_TOMALTA_N]
set_property IOSTANDARD LVDS_25 [get_ports i_FMC_FROMMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports i_FMC_FROMMALTA_N]
set_property IOSTANDARD LVDS_25 [get_ports o_CLK_TOMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports o_CLK_TOMALTA_N]
set_property IOSTANDARD LVDS_25 [get_ports o_TRIG_TOMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports o_TRIG_TOMALTA_N]
set_property IOSTANDARD LVDS_25 [get_ports o_VPULSE_TOMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports o_VPULSE_TOMALTA_N]

#RC added diff term on VPULSE and Trigger. moved from TRUE to trueset_property DIFF_TERM true [get_ports o_CLK_TOMALTA_N]
set_property DIFF_TERM true [get_ports o_CLK_TOMALTA_P]
set_property DIFF_TERM true [get_ports i_FMC_FROMMALTA_N]
set_property DIFF_TERM true [get_ports i_FMC_FROMMALTA_P]
set_property DIFF_TERM true [get_ports o_FMC_TOMALTA_N]
set_property DIFF_TERM true [get_ports o_FMC_TOMALTA_P]
set_property DIFF_TERM true [get_ports o_TRIG_TOMALTA_N]
set_property DIFF_TERM true [get_ports o_TRIG_TOMALTA_P]
set_property DIFF_TERM true [get_ports o_VPULSE_TOMALTA_N]
set_property DIFF_TERM true [get_ports o_VPULSE_TOMALTA_P]
















#### below froim Virtex

#set_property PACKAGE_PIN D36 [get_ports o_PWR_DIG]
#set_property PACKAGE_PIN G32 [get_ports o_PWR_ANA]
#set_property PACKAGE_PIN F32 [get_ports o_PWR_LVDS]

##bOARDv3
#set_property PACKAGE_PIN J31 [get_ports o_PWR_DAC]
##

##Boardv2
##set_property PACKAGE_PIN G29 [get_ports o_PWR_DAC]
##

#set_property PACKAGE_PIN E35 [get_ports i_MON_PWR_DIG]
#set_property PACKAGE_PIN Y29 [get_ports i_MON_PWR_ANA]
#set_property PACKAGE_PIN Y30 [get_ports i_MON_PWR_LVDS]
#set_property PACKAGE_PIN E34 [get_ports i_MON_PWR_DAC]
#set_property IOSTANDARD LVCMOS18 [get_ports o_PWR_DIG]
#set_property IOSTANDARD LVCMOS18 [get_ports o_PWR_ANA]
#set_property IOSTANDARD LVCMOS18 [get_ports o_PWR_LVDS]
#set_property IOSTANDARD LVCMOS18 [get_ports o_PWR_DAC]
#set_property IOSTANDARD LVCMOS18 [get_ports i_MON_PWR_DIG]
#set_property IOSTANDARD LVCMOS18 [get_ports i_MON_PWR_ANA]
#set_property IOSTANDARD LVCMOS18 [get_ports i_MON_PWR_LVDS]
#set_property IOSTANDARD LVCMOS18 [get_ports i_MON_PWR_DAC]


#set_property PACKAGE_PIN D35 [get_ports o_RST_TOMALTA]
#set_property IOSTANDARD LVCMOS18 [get_ports o_RST_TOMALTA]


##Bob board V3
##P32/LA33/U31/T31
#set_property PACKAGE_PIN U31 [get_ports i_FMC_FROMMALTA_P]  
#set_property PACKAGE_PIN T31 [get_ports i_FMC_FROMMALTA_N]
###

##Bob board v2
##set_property PACKAGE_PIN P21 [get_ports i_FMC_FROMMALTA_P]
##set_property PACKAGE_PIN N21 [get_ports i_FMC_FROMMALTA_N]
###

#set_property PACKAGE_PIN J30 [get_ports o_FMC_TOMALTA_P]
#set_property PACKAGE_PIN H30 [get_ports o_FMC_TOMALTA_N]
#set_property PACKAGE_PIN L32 [get_ports o_CLK_TOMALTA_N]
#set_property PACKAGE_PIN M32 [get_ports o_CLK_TOMALTA_P]

##BoardV2
#set_property PACKAGE_PIN H26 [get_ports o_TRIG_TOMALTA_N]
#set_property PACKAGE_PIN H25 [get_ports o_TRIG_TOMALTA_P]
##

##BoardV3
#set_property PACKAGE_PIN K30 [get_ports o_TRIG_TOMALTA_N]
#set_property PACKAGE_PIN K29 [get_ports o_TRIG_TOMALTA_P]
##

#set_property PACKAGE_PIN N31 [get_ports o_VPULSE_TOMALTA_N]
#set_property PACKAGE_PIN P30 [get_ports o_VPULSE_TOMALTA_P]
#set_property IOSTANDARD LVDS [get_ports o_FMC_TOMALTA_P]
#set_property IOSTANDARD LVDS [get_ports o_FMC_TOMALTA_N]
#set_property IOSTANDARD LVDS [get_ports i_FMC_FROMMALTA_P]
#set_property IOSTANDARD LVDS [get_ports i_FMC_FROMMALTA_N]
#set_property IOSTANDARD LVDS [get_ports o_CLK_TOMALTA_P]
#set_property IOSTANDARD LVDS [get_ports o_CLK_TOMALTA_N]
#set_property IOSTANDARD LVDS [get_ports o_TRIG_TOMALTA_P]
#set_property IOSTANDARD LVDS [get_ports o_TRIG_TOMALTA_N]
#set_property IOSTANDARD LVDS [get_ports o_VPULSE_TOMALTA_P]
#set_property IOSTANDARD LVDS [get_ports o_VPULSE_TOMALTA_N]

##RC added diff term on VPULSE and Trigger. moved from TRUE to trueset_property DIFF_TERM true [get_ports o_CLK_TOMALTA_N]
#set_property DIFF_TERM true [get_ports o_CLK_TOMALTA_P]
#set_property DIFF_TERM true [get_ports i_FMC_FROMMALTA_N]
#set_property DIFF_TERM true [get_ports i_FMC_FROMMALTA_P]
#set_property DIFF_TERM true [get_ports o_FMC_TOMALTA_N]
#set_property DIFF_TERM true [get_ports o_FMC_TOMALTA_P]
#set_property DIFF_TERM true [get_ports o_TRIG_TOMALTA_N]
#set_property DIFF_TERM true [get_ports o_TRIG_TOMALTA_P]
#set_property DIFF_TERM true [get_ports o_VPULSE_TOMALTA_N]
#set_property DIFF_TERM true [get_ports o_VPULSE_TOMALTA_P]














